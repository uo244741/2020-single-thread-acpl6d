/*
 * Main.cpp
 *  Contraste ampliado. Contrast stretch
 * PL6-D
 * Laura Marcela Sánchez Peláez 
 * Pedro Zahonero Mangas
 * Hugo Diaz Noriega
 *
 *  Created on: Fall 2020
 */
 
#include <stdio.h>
#include <math.h>
#define cimg_display  1
#include <CImg.h>
#define NTIMES 20
using namespace cimg_library;

// Data type for image components <float>
typedef float data_t;

const char* SOURCE_IMG      = "bailarina.bmp";
const char* DESTINATION_IMG = "bailarina2.bmp";


int main() {


	/***************************************************
			Open file and object initialization
	 ************************************************/

	CImg<float> A(SOURCE_IMG); // Image A

	//Image A
	float *pRsrc, *pGsrc, *pBsrc; // Pointers to the R, G and B components
	int width, height; // Width and height of the image
	int nComp; // Number of image components

	//Image C (Salida)
	float *pRnewC, *pGnewC, *pBnewC;
	float *pdstImageC; // Pointer to the new image pixels

	/***************************************************
			Variables initialization.
	 ************************************************/

	A.display(); // Displays the source image
	width  = A.width(); // Getting information from the source image
	height = A.height();
	nComp  = A.spectrum(); // source image number of components
				// Common values for spectrum (number of image components):
				//  B&W images = 1
				//	Normal color images = 3 (RGB)
				//  Special color images = 4 (RGB and alpha/transparency channel)

	// Allocate memory space for destination image components
	pdstImageC = (float *) malloc (width * height * nComp * sizeof(float));
	if (pdstImageC == NULL) {
		perror("Allocating destination image");
		exit(-2);
	}
	// Pointers to the RGB arrays of the Image A
	pRsrc = A.data(); // pRcomp points to the R component
	pGsrc = pRsrc + height * width; // pGcomp points to the G component
	pBsrc = pGsrc + height * width; // pBcomp points to B component

	// Pointers to the RGB arrays of the destination image
	pRnewC = pdstImageC;// pRcomp points to the R component
	pGnewC= pRnewC + height * width;// pGcomp points to the G component
	pBnewC= pGnewC + height * width;// pBcomp points to B component


	/***********************************************
	 * Algorithm start.
	 * Measure initial time
	 ************************************************/

	struct timespec tStart, tEnd;
	double tiempo;
	// clock_gettime(CLOCK_REALTIME,&tStart);
	if(clock_gettime(CLOCK_REALTIME, &tStart)==-1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	/************************************************
	 * We calculate the maximum and minimum of each RGB
	 *
	*********************************************** */
	
	int npixels = width * height;
	float rmin = 255; 
	float gmin = 255; 
	float bmin = 255; 
	float rmax = 0; 
	float gmax = 0; 
	float bmax = 0; 

	for(int w = 0; w<NTIMES;w++){
		for(int i = 0; i<npixels; i++){  

			float r = *(pRsrc + i);
			float g = *(pGsrc + i);
			float b = *(pBsrc + i);

			if (rmin > r) rmin = r;
			if (gmin > g) gmin = g;
			if (gmin > b) bmin = b;

			if (rmax < r) rmax = r;
			if (gmax < g) gmax = g;
			if (bmax < b) bmax = b;
			
		}
				// *(pdest + i)  --> byte x byte 
			for(int i = 0; i<npixels;i++){

				*(pRnewC + i)= ((*(pRsrc +i)- rmin)/(rmax-rmin))*255;
				*(pGnewC + i)=((*(pGsrc +i)- gmin)/(gmax-gmin))*255;
				*(pBnewC + i)=((*(pBsrc +i)- bmin)/(bmax-bmin))*255;

				if(*(pRnewC + i)>255){
					*(pRnewC + i)=255;
				}
				if(*(pRnewC + i)<0){
					*(pRnewC + i)=0;
				}
				if(*(pGnewC + i)>255){
					*(pGnewC + i)=255;
				}
				if(*(pGnewC + i)<0){
					*(pGnewC + i)=0;
				}
				if(*(pBnewC + i)>255){
					*(pBnewC + i)=255;
				}
				if(*(pBnewC + i)<0){
					*(pBnewC + i)=0;
				}

			}
		}

	/***********************************************
	 * 		End of the algorithm.
	 *   - Measure the end time
	 *   - Calculate the elapsed time
	 ************************************************/
	 
	//clock_gettime(CLOCK_REALTIME,&tEnd);
	if(clock_gettime(CLOCK_REALTIME, &tEnd)==-1)
	{
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	tiempo = (tEnd.tv_sec - tStart.tv_sec );
	tiempo += (tEnd.tv_nsec - tStart.tv_nsec )/1e+9;
	printf("Elapsed time    :%f \n",tiempo);

	// Create a new image object with the calculated pixels
	// In case of normal color images use nComp=3,
	// In case of B/W images use nComp=1.
	CImg<float> dstImageC(pdstImageC, width, height, 1, nComp);

	// Store destination image in disk
	dstImageC.save(DESTINATION_IMG); 

	// Display destination image
	dstImageC.display();
	return 0;
}